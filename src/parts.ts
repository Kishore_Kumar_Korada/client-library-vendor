import { BrokerCore, DefaultCollectionLimit } from "./core";  
import { UploadProgressCallback, Collection, IdCollection } from "./utils";
import * as faker from "faker";
import { mockUploadFile } from "./mock/upload";
import { createPart, createModelInfo } from "./data/parts";
import { createArray } from "./data/common";

export class PartsBroker {
    constructor(private _core: BrokerCore) { }

    create(part?: PartDescriptor) {
        return Promise.resolve({ _id: faker.random.uuid() });
    }

    update(part: PartDescriptor) {
        return Promise.resolve();
    }

    get(partId: string): Promise<Part> {
        return Promise.resolve(createPart(partId));
    }

    listAll(offset = 0, limit = DefaultCollectionLimit): Promise<IdCollection> {
        let parts = createArray(2, 10, faker.random.uuid) as IdCollection;
        parts.totalCount = parts.length;
        return Promise.resolve(parts);
    }

    listByTags(tags: string[], offset = 0, limit = DefaultCollectionLimit): Promise<IdCollection> {
        let parts = createArray(2, 5, faker.random.uuid) as IdCollection;
        parts.totalCount = parts.length;
        return Promise.resolve([]);
    }

    addModel(partId: string, file: File, progressCallback?: UploadProgressCallback) {
        return mockUploadFile(file, progressCallback);
    }

    replaceModel(partId: string, modelId: string, file: File, progressCallback?: UploadProgressCallback) {
        return mockUploadFile(file, progressCallback);
    }

    removeModel(partId: string, modelId: string) {
        return Promise.resolve();
    }
    
    getModelInfo(modelId: string): Promise<ModelInfo> {
        return Promise.resolve(createModelInfo(modelId));
    }
}

export class PartDescriptor {
    _id?: string;
    tags?: string[];
    name?: string;
    desc?: string;
    grade?: string;
    material?: string;
    displayUnit?: string;
    tolerance?: number;
}

export class Part {
    _id: string;
    tags: string[];
    name: string;
    desc: string;
    thumbnailUrl: string;
    grade: string;
    material: string;
    displayUnit: string;
    tolerance: number;
    createdDate: Date;
    status: PartStatus;
    // refs: fileModelId
    models: string[];
}

export enum PartStatus {
    Zygote,
    Unused,
    Active,
}

export class ModelInfo {
    _id: string;
    url: string;
    srcName: string;
    updateDate: Date;
    type: "2d" | "3d";
}