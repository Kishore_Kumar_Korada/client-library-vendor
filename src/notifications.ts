import { BrokerCore, DefaultCollectionLimit } from "./core";
import { Collection } from "./utils";

export class NotificationsBroker {
    constructor(private _core: BrokerCore) {}

    // TODO: Kishore
    // Generate fakes
    getAll(offset = 0, limit = DefaultCollectionLimit): Promise<Notifications> {
        return Promise.resolve([]);
    }

    markSeen(notificationId: string): Promise<void> {
        return Promise.resolve();
    }
}

export class Notification {
    _id: string;
    date: Date;
    message: string;    
    thumbnailUrl: string;
    actionUrl: string;
    state: NotificationState;    
    // Reserved for future use.
    type: any;
}

export type Notifications = Collection<Notification>;

export enum NotificationState {
    New,
    Seen,
}