export class Collection<T> extends Array<T> {
    totalCount: number;
}

export type IdCollection = Collection<string>;

export type UploadProgressCallback = (percent: number, bytes: number) => void;