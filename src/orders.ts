import { TelemetryBroker } from '../lib/telemetry';
import * as faker from "faker";
import { BrokerCore, DefaultCollectionLimit } from "./core";  
import { UploadProgressCallback, Collection, IdCollection } from "./utils";
import { createArray } from "./data/common";
import { Address } from "./user";
import { createOrderForEstimation } from "./data/orders"
import { Part } from "./parts";

export class OrdersBroker {
    constructor(private _core: BrokerCore) { }

    /*
        This method is for getting all orders that are open for estimations from vendor management
    */
    getAllOrderEstimations(offset = 0, limit = DefaultCollectionLimit): Promise<OrderEstimation[]> {
        let listOfOrdersForEstimation = createArray(1,3,createOrderForEstimation) as OrderEstimations;
        return Promise.resolve(listOfOrdersForEstimation);
    }

    updateOrderEstimation(order: OrderPriceEstimationDescriptor): Promise<void> {
        return Promise.resolve();
    }

    listByState(orderQueryState = OrderQueryState.All, offset = 0, limit = DefaultCollectionLimit): Promise<IdCollection> {
        let parts = createArray(2, 5, faker.random.uuid) as IdCollection;
        parts.totalCount = parts.length;
        return Promise.resolve([]);
    }

    addVendorQuote(vedorQuote: VendorQuoteDescriptior): Promise<void> {
        return Promise.resolve();
    }

    getAllVendorQuotes(offset = 0, limit = DefaultCollectionLimit): Promise<VedorQuote[]> {
        return Promise.resolve(null);
    }


    getActivityLog(orderId: string): Promise<any> {
        return Promise.resolve();
    }

}

export class OrderEstimation {
    _id:string;  //Ref to orderId
    state:OrderState;
    parts: EstimatedOrderPartInfo[]
}

export type OrderEstimations = Collection<OrderEstimation>;

export class OrderEstimatioinDescriptor {
    _id:string; //Ref to orderId
    state:OrderState;
}

export enum OrderState {
    Unknown = -1, // Special status for manual intervention at any point    
    Initial,
    Pricing,
    Priced, // Pricing done, waiting for user approval
    Placed, // Order placed, awaiting payment resolution
    Processed, // Producer orders have now been created
    Production, // Producer orders have been acknowledged
    Shipment, // Orders have been shipped
    Delivered, 
    Cancelled, 
    Suspended, 
}

export enum OrderQueryState {
    All,    
    Active,
    Quotes,
    Completed,
    RequiresAttention,
}


export class EstimatedOrderPartInfo extends Part {
    quantity: number;
}

export enum OrderType {
    Normal,
    Expedited,
    Special,
}

export class CostElement {
    rawMaterial:number;//cost for raw material
    setup:number;// cost for setup
    others:number;// cost for others
}

export class Process {
    _id:string;
    name:string;//eg: material procurement 
}

export class ProcessCost extends Process {
    cost:number;
}

export class PartPriceInfo {
    _id:string;//ref to partId
    costElement:CostElement;
    processCost:ProcessCost;
}

export class OrderPriceEstimationDescriptor {
    _id:string;
    parts: PartPriceInfo[];
    totalCost:number;
}

export class Vendor {
    _id:string;
    name:string;
    address:Address;
}

export class VendorQuoteDescriptior {
    _id:string;//ref to PartId
    vendors:string[];//ref id's for vendors
}

export class VedorQuote {

}