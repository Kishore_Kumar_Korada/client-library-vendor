import { BrokerCore } from "./core";
import { User } from "./user";
import { EventEmitter } from "eventemitter3";
import * as faker from "faker";
import mockAuth from "./mock/auth";

/**
 * Account broker.
 *
 * Whenever an authentication status change happens, a "statuschange" event is emitter.
 * If the 'suppressEvents' option is true when calling any of the methods that could result in a change,
 * then the status change events are skipped.
 *
 * IceBox:
 *  - getLoginInfo: with security details of all
 *      logins for the user id
 *
 * @export
 * @class AccountBroker
 */
export class AccountBroker extends EventEmitter {

    constructor(private _core: BrokerCore) { 
        super();
    }

    static StatusChangeEvent = "statusChange";
    
    defaultOptions = {
        setTokens: true,
        suppressEvents: false,
    };

    create(email: string, verficationUrl: string, opts = this.defaultOptions) {
        return Promise.resolve({
            _id: faker.random.uuid()
        });
    }

    createCsrfToken(scope: CsrfTokenScope, opts = this.defaultOptions) {
        return Promise.resolve({ token: faker.random.uuid(), scope: CsrfTokenScope.ApiBroker });
    }

    logInWithRegistrationToken(token: string, userId: string, opts = this.defaultOptions) {
        mockAuth.logIn();
        emitStatusChangeIfRequired(this, opts);        
        return Promise.resolve({ isValid: true, token: faker.random.uuid() });
    }

    logInWithPassword(email: string, password: string, opts = this.defaultOptions) {
        mockAuth.logIn();
        emitStatusChangeIfRequired(this, opts);
        return Promise.resolve({ _id: faker.random.uuid(), token: faker.random.uuid() });
    }

    logOut(opts = this.defaultOptions) {
        mockAuth.logOut();        
        emitStatusChangeIfRequired(this, opts);        
        return Promise.resolve();
    }

    get isLoggedIn() {
        return mockAuth.isLoggedIn();
    }
}

function emitStatusChangeIfRequired(emitter: EventEmitter, opts: any) {
    let suppressEvents = false;
    if (opts && opts.suppressEvents) suppressEvents = true;
    if (!suppressEvents) emitter.emit(AccountBroker.StatusChangeEvent);
}

export enum CsrfTokenScope {
    ApiBroker,
}