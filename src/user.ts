import { BrokerCore } from "./core";  
import { createUser, createAddress } from "./data/user";
import { createArray } from "./data/common";
import { UploadProgressCallback } from "./utils";
import * as faker from "faker";
import { mockUploadFile } from "./mock/upload";

/**
 * The user broker
 * @export
 * @class UserBroker
 */
export class UserBroker {
    constructor(private _core: BrokerCore) { }

    get(): Promise<User> {
        return Promise.resolve(createUser());
    }

    update(user: UserDescriptor) {
        return Promise.resolve();
    }

    updatePicture(file: File, progressCallback?: UploadProgressCallback) {
        return mockUploadFile(file, progressCallback);
    }

    getAddresses(): Promise<Address[]> {
        return Promise.resolve(createArray(0, 2, createAddress));
    }

    getGroupInfo(groupId: string): Promise<UserGroup> {
        return Promise.resolve({
            _id: groupId,
            name: faker.name.findName(),
            type: UserGroupType.Company
        });
    }
}

export class UserDescriptor {
    _id?: string;
    email?: string;
    name?: string;
    role?: string;
    password?: string;
}

export class User {
    _id: string;
    email: string;
    name: string;
    role: string;
    createdDate: Date;
    pictureUrl: string;
    // refs: groupId
    groups: string[];
}

export class UserGroup {
    _id: string;
    name: string;
    type: UserGroupType;
}

export enum UserGroupType {
    Company,
    Team,
    Contextual
}

export class Address {
    _id: string;
    street: string;
    city: string;
    state: string;
    zip: string;
    country: string;
    tags: AddressTags[];
}

export enum AddressTags {
    Preferred,
}