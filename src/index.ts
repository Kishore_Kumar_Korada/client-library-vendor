import { BrokerCore } from "./core";
import { AccountBroker } from "./account";
import { UserBroker } from "./user";
import { SearchBroker } from "./search";
import { PartsBroker } from "./parts";
import { OrdersBroker } from "./orders";
import { TelemetryBroker } from "./telemetry";
import { NotificationsBroker } from "./notifications";

/**
 * The Api Broker
 *
 * @export
 * @class ApiBroker
 */
export class ApiBroker {
    private _core: BrokerCore = null;
    private _account: AccountBroker = null;
    private _user: UserBroker = null;
    private _search: SearchBroker = null;
    private _orders: OrdersBroker = null;
    private _parts: PartsBroker = null;
    private _telemetry: TelemetryBroker = null;
    private _notifications: NotificationsBroker = null;

    constructor() {
        this._core = new BrokerCore(null);
    }

    setApiToken(token: string) {}
    setCsrfToken(token: string) {}
    setRegistrationToken(token: string, userId: string) {}

    get account() {
        return this._account || (this._account = new AccountBroker(this._core));
    }

    get user() {
        return this._user || (this._user = new UserBroker(this._core));
    }

    get search() {
        return this._search || (this._search = new SearchBroker(this._core));
    }
    
    get orders() {
        return this._orders || (this._orders = new OrdersBroker(this._core));
    }

    get parts() {
        return this._parts || (this._parts = new PartsBroker(this._core));
    }

    get notifications() {
        return this._notifications || (this._notifications = new NotificationsBroker(this._core));
    }

    get telemetry() {
        return this._telemetry || (this._telemetry = new TelemetryBroker(this._core));
    }
}