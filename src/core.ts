/**
 * This is the client core that does most of the
 * heavy-lifting. Persists auth-tokens, csrf tokens,
 * and makes the actual http requests with appropriate
 * connection pooling, priorities and cancellations.
 * 
 * @export
 * @class ClientCore
 */

export const DefaultCollectionLimit = 20;

export class BrokerCore {
    _csrfToken: string;
    _authToken: string;
    _httpClient: any;

    constructor(baseUrl: string) {}

    get: RequestFn;
    getAnonymous: RequestFn;
    post: RequestFn;
    postAnonymous: RequestFn;
    headAnonymous: RequestFn;
    head: RequestFn;
    putAnonymous: RequestFn;
    put: RequestFn;
    patchAnonymous: RequestFn;
    patch: RequestFn;
    deleteAnonymous: RequestFn;
    delete: RequestFn;
}

export type RequestFn = () => void;

function makeBrokerCorePrototype(brokerCore: Function) {
    ["get", "head", "post", "put", "patch", "delete"].forEach(x => {
        let anonymousKey = x + "Anonymous";
        brokerCore.prototype[x] = () => { };
        brokerCore.prototype[anonymousKey] = () => { };
    });
}

makeBrokerCorePrototype(BrokerCore);