class ApiError extends Error {
    httpResponseCode: number;
    private _kind: ErrorKind;
    
    get kind() {
        return this._kind
    };
}

export enum ErrorKind {
    Unknown,
    NetworkError,
    ClientError,    
    ServerError,
    ApplicationError,
}

export enum ClientSecurityErrorKind {
    Blocked,
    // Temporarily blocked. Try again later.
    TemporarilyBlocked,
    // Client is sending too many requests. Back off now, so that it isn't blocked,
    // for a given time, and try again later.
    RateLimitExceeded,
    // A verification like CAPTCHA, etc is required in order to proceed.
    VerficationRequired,
    // Standard Http errors
    Unauthorized,
    Forbidden,
}