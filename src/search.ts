import { BrokerCore, DefaultCollectionLimit } from "./core";
import { Collection } from "./utils";

export class SearchBroker {
    constructor(private _core: BrokerCore) {}

    query(query: string, context: SearchContext,
        offset = 0, limit = DefaultCollectionLimit): Promise<SearchResult> {
        return Promise.resolve([]);
    }
}

export enum SearchContext {
    All,
    Parts,    
    Orders,
    OrderItem,
    // User settings and profile
    User,
    // Specific user tasks
    Tasks,
    // Natural language interface.
    Natural,
}

export type SearchResult = Collection<SearchResultItem>;

export class SearchResultItem {
    type: SearchResultItemType;
    value: any;
}

export enum SearchResultItemType {
    Orders,
    Parts,
}