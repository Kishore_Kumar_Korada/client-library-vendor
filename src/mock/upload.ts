import * as faker from "faker";

export function mockUploadFile(file: File, progressCallback: (bytes: number, percent: number) => void) {
    const totalDelay = 2000;
    const ticks = 100;
    const timerDelay = totalDelay / ticks;
    let currentTick = 1;
    let reportProgress = () => {
        let percent = currentTick / ticks;
        currentTick++;
        progressCallback(percent, (file.size / 100) * percent);
    };

    return new Promise((res) => {
        if (progressCallback == null) {
            res({ url: faker.image.avatar() });
        } else {
            let timerId = setInterval(reportProgress, ticks);
            setTimeout(() => {
                clearInterval(timerId);
                progressCallback(100, file.size);
                res({ url: faker.image.avatar() });
            }, totalDelay);
        }
    });
}