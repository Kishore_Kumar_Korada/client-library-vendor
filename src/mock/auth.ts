export class MockAuthStateProvider {
    private _isLoggedIn = false;
    
    logIn() {
        this._isLoggedIn = true;   
    }

    logOut() {
        this._isLoggedIn = false;
    }

    isLoggedIn() {
        return this.isLoggedIn;
    }
}

export default new MockAuthStateProvider();