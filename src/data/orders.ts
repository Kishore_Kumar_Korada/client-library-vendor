import { OrderState } from '../orders';
import { Address, AddressTags } from '../user';
import * as faker from "faker";
import * as userData from "./user";

import * as orders from '../orders';
import { createArray } from "./common";
import { PartStatus } from "../parts"


export function createOrderForEstimation() : orders.OrderEstimation {
    return {
        _id: faker.random.uuid(),
        state:faker.random.number({
            min:OrderState.Initial,
            max:OrderState.Suspended
        }),
        parts:createArray(0,3,createOrderPartInfo)
    }
}

export function createOrderPartInfo() : orders.EstimatedOrderPartInfo {
    return {
        _id: faker.random.uuid(),
        createdDate: faker.date.past(10),
        desc: faker.lorem.sentences(faker.random.number({ min: 1, max: 3 })),
        displayUnit: faker.random.arrayElement(["mm", "inches"]),
        grade: faker.random.word().toUpperCase(),
        material: faker.commerce.productMaterial(),
        status: PartStatus.Unused,
        tags: createArray(0, 3, () => faker.commerce.productAdjective()),
        name: faker.commerce.productName(),
        thumbnailUrl: faker.image.avatar(),
        tolerance: faker.random.number({ min: 0, max: 1, precision: 6 }),
        models: createArray(0, 4, () => faker.internet.url()),
        quantity: faker.random.number(10)
    }
}

export function createVendorInfo() : orders.Vendor {
    return {
        _id:faker.random.uuid(),
        name:faker.lorem.word(),
        address:userData.createAddress()
    }
}





