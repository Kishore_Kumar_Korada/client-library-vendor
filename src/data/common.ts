import * as faker from "faker";

export function createArray<T>(min: number, max: number, factory: () => T) {
    let n = faker.random.number({ min, max });
    return Array(n).fill(0).map(factory);
}