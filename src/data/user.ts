import * as faker from "faker";
import { User, UserGroupType, Address, AddressTags } from "../user";

export function createUser() : User {
    return {
        _id: faker.random.uuid(),
        createdDate: faker.date.past(10),
        email: faker.internet.email(),
        name: `${faker.name.firstName()} ${faker.name.lastName()}`,
        groups: [faker.random.uuid()],
        role: faker.name.jobDescriptor(),
        pictureUrl: faker.image.avatar(),
    }
}

export function createAddress(): Address {
    return {
        _id: faker.random.uuid(),
        country: faker.address.country(),
        city: faker.address.city(),
        state: faker.address.state(),
        street: faker.address.streetName(),
        zip: faker.address.zipCode(),
        tags: [],
    };
}