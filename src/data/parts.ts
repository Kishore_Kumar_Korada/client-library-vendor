import * as faker from "faker";
import { Part, ModelInfo, PartStatus } from "../parts";
import { createArray } from "./common";

export function createPart(partId: string) : Part {
    return {
        _id: partId || faker.random.uuid(),
        createdDate: faker.date.past(10),
        desc: faker.lorem.sentences(faker.random.number({ min: 1, max: 3 })),
        displayUnit: faker.random.arrayElement(["mm", "inches"]),
        grade: faker.random.word().toUpperCase(),
        material: faker.commerce.productMaterial(),
        status: PartStatus.Active,
        tags: createArray(0, 3, () => faker.commerce.productAdjective()),
        name: faker.commerce.productName(),
        thumbnailUrl: faker.image.avatar(),
        tolerance: faker.random.number({ min: 0, max: 1, precision: 6 }),
        models: createArray(0, 4, () => faker.internet.url()),
    }
}

export function createModelInfo(modelId?: string): ModelInfo {
    return {
        _id: modelId || faker.random.uuid(),
        srcName: (faker.system.fileName as any)(),
        type: faker.random.arrayElement(["2d", "3d"]) as any,
        updateDate: faker.date.recent(15),
        url: faker.image.abstract(),
    }
}