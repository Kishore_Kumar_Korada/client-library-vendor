import * as brokerLib from "../";

async function run() {
    let api = new brokerLib.ApiBroker();
    let estimatedOrders = await api.orders.get();
    console.log(estimatedOrders);
}

run();