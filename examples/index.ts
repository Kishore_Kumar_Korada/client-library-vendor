import * as brokerLib from "../";
import { BrokerCore, DefaultCollectionLimit } from "../src/core";
import { UploadProgressCallback, Collection, IdCollection } from "../src/utils";
import * as faker from "faker";
import { createArray } from "../src/data/common";
import { createOrderForEstimation } from "../src/data/orders"

async function run() {
    let api = new brokerLib.ApiBroker();
    let user = await api.user.get();
    console.log(user);
}

async function listAll(offset = 0, limit = DefaultCollectionLimit) {
    let parts = createArray(2, 10, faker.random.uuid) as IdCollection;
    parts.totalCount = parts.length;
    console.log("Parts: "+parts);
}

async function listAll(offset = 0, limit = DefaultCollectionLimit) {
    let parts = createArray(2, 10, createOrderForEstimation);
    parts.totalCount = parts.length;
    console.log("Parts: "+parts);
}


listAll();
